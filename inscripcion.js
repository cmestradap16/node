const { argv } = require('./CursosDisponibles')
const fs= require('fs')
const {cursos} = require('./Cursos')  

//Inicio creacion documento
function creacionDocumento(argv) {
    let fnd = cursos.find(function(curso) {
        return curso.id==argv.id;
    })
    if(fnd== undefined){
        cursos.forEach(element => {
            console.log('El curso ' + element.nombre + ' identificado con el id: ' + element.id + ' tiene una duracion de '
                    + element.duracion + ' horas y un valor de ' + element.valor);
        });
    }else{
    console.log('Se ha creado la solicitud al señor(a) '+ argv.nombre);
    documentoTxt(argv,fnd);
    }
}

function documentoTxt(argv,fnd){
texto = 'El estudiante ' + argv.nombre + ' con cedula: ' + argv.cedula + ' se ha matriculado en el curso llamado ' + fnd.nombre
     + ' tiene una duracion de ' + fnd.duracion + ' y un valor de ' + fnd.valor;
fs.writeFile('resumen.txt', texto, (err)=>{
    if(err) throw (err);
    console.log('Se creo el archivo correctamente')
})
}

module.exports = {  
    creacionDocumento
}