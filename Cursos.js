// Cursos disponibles
let cursos = [{
    id: 1,
    nombre: 'Angular 6',
    duracion: 64,
    valor: 32000
},
{
    id: 2,
    nombre: 'Fundamentos de Programacion',
    duracion: 120,
    valor: 120000
},
{
    id: 3,
    nombre: 'Node JS',
    duracion: 50,
    valor: 90000
}];

module.exports = {
    cursos
}