const { cursos } = require('./Cursos')
const { opciones } = require('./parametrosInscripcion')
const { creacionDocumento } = require('./inscripcion')

// Inscripcion de los cursos
const argv = require('yargs')
    .command('inscribir', 'inscribirme en un curso', opciones)
    .argv;

//Mostrar todos los cursos disponibles
function mostrarCursos(tiempoLapso) {
    time = 0;
    cursos.forEach(element => {
        setTimeout(function () {
            console.log('El curso ' + element.nombre + ' identificado con el id: ' + element.id + ' tiene una duracion de '
                + element.duracion + ' horas y un valor de ' + element.valor);
        }, time += tiempoLapso);
    });
}

//Definir accion
if (argv.id == undefined) {
    mostrarCursos(2000); 
}else if(argv.nombre != undefined && argv.id != undefined && argv.cedula != undefined){
    creacionDocumento(argv);
}

module.exports = {
    argv,
    mostrarCursos
}